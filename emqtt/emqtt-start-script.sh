#!/bin/bash

export REPOSITORY_EMQTT="https://bitbucket.org/meshstack/deployment-scripts-emqtt/raw/HEAD/emqtt"
export REPOSITORY_MONIT="https://bitbucket.org/meshstack/deployment-scripts-monit/raw/HEAD/monit"

wget $REPOSITORY_EMQTT/emqtt-template.sh
chmod +x emqtt-template.sh
./emqtt-template.sh
